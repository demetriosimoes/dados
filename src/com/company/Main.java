package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	    Dados dado = new Dados();
	    dado.setQtdeFaces(20);
        int somaParcial=0, somaTotal = 0;
        ArrayList<Integer> listaDados = new ArrayList<Integer>();

        int loop =3;
        for (int j=0;j<loop;j++) {
            listaDados = dado.getValorSorteado(3);
            for (int i = 0; i < 3; i++) {
                int dados = listaDados.get(i);
                somaParcial += dados;
                System.out.println("valor sorteio no." + (j+1) + ": "+ dados);
            }
            System.out.println("somaParcial:  " + somaParcial + "\n");
            somaTotal +=somaParcial;
            somaParcial =0 ;
        }
        System.out.println("somaTotal:  " + somaTotal);

    }
}
