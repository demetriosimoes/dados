package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Dados {

    private ArrayList<Integer> dadoArray = new ArrayList<Integer>();
    private String valorSorteadoDado;
    private int qtdeFaces;

    public Dados() {

    }

    public int getQtdeFaces() {
        return qtdeFaces;
    }

    public void setQtdeFaces(int qtdeFaces) {
        this.qtdeFaces= qtdeFaces;
        for (int i =1 ; i<=qtdeFaces;i++){
            this.dadoArray.add (i);
        }
    }

    public int getValorSorteadoDado() {
        Random gerador = new Random();
        int i = gerador.nextInt(this.qtdeFaces);
        return this.dadoArray.get(i);
    }

    public ArrayList<Integer> getValorSorteado(int iqtdesorteios)
    {
        Random gerador = new Random();
        int j=0;
        for(int i=0;i<iqtdesorteios;i++) {
            j = gerador.nextInt(this.qtdeFaces);
            dadoArray.set(i,j);
        }
        return this.dadoArray;
    }

}
